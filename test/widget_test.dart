import 'package:am_i_indoors/experiment/model_tester.dart';
import 'package:am_i_indoors/model/discrete_model.dart';
import 'package:am_i_indoors/model/guess_result.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('Test experiment with satellites model', () {
    DiscreteModel satellitesModel = DiscreteModel();
    satellitesModel.addDecider((p0) => p0.satellitesCount >= 3 ? GuessResult.outdoors : GuessResult.indoors);
    ModelTester tester = ModelTester();
    tester.testModel(satellitesModel);
  });
}
