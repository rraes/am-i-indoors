import 'package:am_i_indoors/data/sensor_datum.dart';
import 'package:am_i_indoors/model/guess_result.dart';
import 'package:am_i_indoors/model/guessing_model.dart';

class DiscreteModel extends GuessingModel {
  final List<GuessResult Function(SensorDatum)> _deciders = [];

  @override
  GuessResult guess(SensorDatum data) {
    List<GuessResult> results = [];
    for (var decider in _deciders) {
      results.add(decider(data));
    }

    int indoorsCount = results.where((element) => element == GuessResult.indoors).length;
    int outdoorsCount = results.where((element) => element == GuessResult.outdoors).length;
    int unknownCount = results.where((element) => element == GuessResult.unknown).length;

    if (outdoorsCount == 0 && unknownCount == 0) {
      return GuessResult.indoors;
    } else if (indoorsCount == 0 && unknownCount == 0) {
      return GuessResult.outdoors;
    }
    return GuessResult.unknown;
  }

  void addDecider(GuessResult Function(SensorDatum) decider) {
    _deciders.add(decider);
  }
}