import 'package:am_i_indoors/data/sensor_datum.dart';
import 'package:am_i_indoors/model/guess_result.dart';

abstract class GuessingModel {
  /// Main method of the experiment.
  /// This will return a guess (indoors/outdoors/unknown) for the input data
  /// regarding current model configuration.
  GuessResult guess(SensorDatum data);
}