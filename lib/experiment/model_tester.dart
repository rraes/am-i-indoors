import 'dart:math';

import 'package:am_i_indoors/data/sensor_datum.dart';
import 'package:am_i_indoors/data/sensor_datum_truth.dart';
import 'package:am_i_indoors/model/guess_result.dart';
import 'package:am_i_indoors/model/guessing_model.dart';
import 'package:intl/intl.dart';


class ModelTester {
  final DateFormat _formatter = DateFormat('yyyy_MM_dd_HH_mm_ss');

  Future<void> testModel(GuessingModel model, {bool verbose = true}) async {
    List<SensorDatumTruth> dataset = await _loadGroundTruthData();
    int correctGuesses = 0;
    int falseGuesses = 0;
    int unknownGuesses = 0;

    if (verbose) print('Running experiment on ${dataset.length} data points...');

    for (var dataPoint in dataset) {
      GuessResult modelResult = model.guess(dataPoint.datum);
      switch(modelResult){
        case GuessResult.indoors:
          dataPoint.isIndoors
              ? correctGuesses += 1
              : falseGuesses += 1;
          break;

        case GuessResult.outdoors:
          dataPoint.isIndoors
              ? falseGuesses += 1
              : correctGuesses += 1;
          break;

        case GuessResult.unknown:
          unknownGuesses += 1;
      }
    }

    if (verbose) {
      print("\nEnd of the experiment!");
      print("    Correct guesses: $correctGuesses (${(correctGuesses*100/dataset.length).toStringAsFixed(2)}%)");
      print("    Incorrect guesses: $falseGuesses (${(falseGuesses*100/dataset.length).toStringAsFixed(2)}%)");
      print("    Unknown guesses: $unknownGuesses (${(unknownGuesses*100/dataset.length).toStringAsFixed(2)}%)\n");
    }
  }

  Future<List<SensorDatumTruth>> _loadGroundTruthData() async {
    List<SensorDatumTruth> data = [];

    for (var i=0; i<1000; i++) {
      SensorDatum randomDatum = SensorDatum.fromRawData(_generateRandomSensorData());
      data.add(
          SensorDatumTruth(
            datum: randomDatum,
            isIndoors: randomDatum.satellitesCount < 3
          )
      );
    }

    return data;
  }

  String _generateRandomSensorData() {
    Random r = Random();
    DateTime randomDate = DateTime.fromMicrosecondsSinceEpoch(r.nextInt(4294967296) + 10000000);
    String generatedData = _formatter.format(randomDate) + ';';

    generatedData += r.nextInt(50).toString() + ';';  // PM1.0
    generatedData += r.nextInt(120).toString() + ';'; // PM2.5
    generatedData += r.nextInt(150).toString() + ';'; // PM10
    generatedData += r.nextInt(150).toString() + ';';
    generatedData += r.nextInt(150).toString() + ';';
    generatedData += r.nextInt(150).toString() + ';';
    generatedData += r.nextInt(150).toString() + ';';
    generatedData += r.nextInt(150).toString() + ';';
    generatedData += r.nextInt(150).toString() + ';';

    bool isIndoors = r.nextBool();
    generatedData += isIndoors ? '0.0;' : (r.nextDouble()*180-90).toString() + ';';    // lat
    generatedData += isIndoors ? '0.0;' :(r.nextDouble()*360-180).toString() + ';';   // lng
    generatedData += isIndoors ? '0.0;' : (r.nextDouble()*100).toString() + ';';       // altitude
    generatedData += (r.nextDouble()*50).toString() + ';';                        // speed
    generatedData += isIndoors ? '0;' : (r.nextInt(8)+3).toString() + ';';               // satellites

    generatedData += (r.nextDouble()*40).toString() + ';';        // temp
    generatedData += (r.nextDouble()*110000).toString()+ ';';     // pressure
    generatedData += (r.nextDouble()*40).toString()+ ';';         // temp
    generatedData += (r.nextDouble()*60).toString()+ ';';         // humidity
    generatedData += (r.nextDouble()*5).toString()+ ';';          // battery level

    generatedData += (r.nextDouble()*40).toString()+ ';';         // temp
    generatedData += (r.nextDouble()*60).toString();              // temp

    return generatedData;
  }
}