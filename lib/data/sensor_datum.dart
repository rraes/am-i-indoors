class SensorDatum {
  final DateTime time;
  final double latitude;
  final double longitude;
  final double altitude;
  final double speed;
  final int satellitesCount;
  final double temperature;
  final double pressure;
  final double humidity;

  SensorDatum({
    required this.time,
    required this.latitude,
    required this.longitude,
    required this.altitude,
    required this.speed,
    required this.satellitesCount,
    required this.temperature,
    required this.pressure,
    required this.humidity
  });

  factory SensorDatum.fromRawData(String data) {
    List<String> information = data.split(';');
    List<String> timeInfo = information[0].split('_');

    return SensorDatum(
        time: DateTime.parse('${timeInfo[0]}-${timeInfo[1]}-${timeInfo[2]} ${timeInfo[3]}:${timeInfo[4]}:${timeInfo[5]}'),
        latitude: double.parse(information[10]),
        longitude: double.parse(information[11]),
        altitude: double.parse(information[12]),
        speed: double.parse(information[13]),
        satellitesCount: int.parse(information[14]),
        temperature: double.parse(information[20]),
        pressure: double.parse(information[16]),
        humidity: double.parse(information[21])
    );
  }
}