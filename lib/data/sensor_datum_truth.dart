import 'package:am_i_indoors/data/sensor_datum.dart';

class SensorDatumTruth {
  final SensorDatum datum;
  final bool isIndoors;

  SensorDatumTruth({
    required this.datum,
    required this.isIndoors
  });
}